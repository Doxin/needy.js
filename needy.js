/*

needy.js: javascript dependencies made easy.

function needs(needlist,callback)
    makes sure all the js files mentioned in needlist are loaded before calling
    the callback
    
by default looks in the js/ folder for scripts. you can change this setting by
changing the BASE variable in this file a few lines below.

*/

/*

The MIT License (MIT)

Copyright (c) 2014 Lieuwe Mosch

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

(function () {
    "use strict";
    var BASE = "js/", module_registry = [];
    var contains = function (a, obj) {
        var i;
        for (i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
                }
            }
        return false;
        }

    window.needs = function (needlst,callback) {
        var donecount = 0;
        var needcount = 0;
        var i;

        callback = callback||function (){}

        var done = function () {
            donecount++;
            if(donecount >= needcount) callback();
            }

        var nneedlst = [];

        for(i = 0; i < needlst.length; i++) {
            var name=needlst[i]
            if(!contains(module_registry,name)) {
                needcount++;
                nneedlst.push(name);
                }
            }

        needlst = nneedlst;

         for(i = 0; i < needlst.length; i++) {
            var name = needlst[i];
            console.log("loading "+name);

            var js = document.createElement("script");
            js.type = "text/javascript";
            if(name.indexOf("://")>-1)
                js.src=name
            else
                js.src = BASE+name;
            js.async = true;

            js.onreadystatechange = done;
            js.onload = done;

            document.head.appendChild(js);
            }
        }

})();
