needy.js
=======

javascript dependencies made easy. needy.js makes sure all the javascript files needed are loaded. nothing more nothing less.

api reference
============

    needs(needlist,callback)

makes sure the files mentioned in needlist are loaded before calling callback. items in needlist can either be URLs or filenames. filenames are prefixed with the BASE variable, "js/" by default.

example usage
============

    <html>
        <head>
            <script src="https://raw.githubusercontent.com/SuperDoxin/needy.js/master/needy.js" type="text/javascript">
            <script type="text/javascript">
                needs(["http://code.jquery.com/jquery-1.11.0.min.js","localfile.js"],function(){
                    //by the time this function is called, both jquery and js/localfile.js are loaded.
                    })
            </script>
        </head>
        <body>
        </body>
    </html>
